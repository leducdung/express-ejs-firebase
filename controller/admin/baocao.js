
  module.exports = function (req, res) {
    const usersRef = require("../../model-dbs/DBAdmin")
      .child("feedback");
    const cookie_username = req.cookies.usernameADMIN;
    var arr = [];
    usersRef.once("value").then(function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        arr.push(Object.values(childSnapshot.val()));
      });
      res.render("admin/baocao", {
        cookie: cookie_username,
        arr,
      });
 
    });
  };
  