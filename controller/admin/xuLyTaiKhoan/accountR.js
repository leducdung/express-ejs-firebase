module.exports = function (req, res) {
  var username_params = req.params.username;
  const usersRef = require("../../../model-dbs/DBuser")
    .child("Recruiter")
    .child("Profile")
    .child(username_params);
  const cookie_username = req.cookies.usernameADMIN;
  usersRef.once("value", (snapshot) => {
    let result = snapshot.val();
    res.render("admin/accountR", {
      cookie: cookie_username,
      result,
    });
  });
};
