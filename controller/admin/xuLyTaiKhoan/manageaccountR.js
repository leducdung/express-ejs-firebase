module.exports = function (req, res) {
  const usersRef = require("../../../model-dbs/DBuser")
    .child("Recruiter")
    .child("Profile");
  const cookie_username = req.cookies.usernameADMIN;
  var arr = [];
  usersRef.once("value").then(function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
      arr.push(childSnapshot.val());
    });
    res.render("admin/manageaccountR", {
      cookie: cookie_username,
      arr,
    });
  });
};
