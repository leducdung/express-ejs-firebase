const usersRef = require("../../../model-dbs/DBuser");
module.exports = function (req, res) {
  const usersRefR = usersRef.child("Volunteer").child("Profile");
  const username = req.params.username;
  usersRefR.child(username).update({
    lock: false,
  });
  res.redirect("/admin/accountV/" + username);
};
