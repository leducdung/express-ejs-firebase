const usersRef = require("../../../model-dbs/DBuser");
module.exports = function (req, res) {
  const usersRefR = usersRef.child("Recruiter").child("Profile");
  const username = req.params.username;
  usersRefR.child(username).update({
    lock: false,
  });

  res.redirect("/admin/accountR/" + username);
};
