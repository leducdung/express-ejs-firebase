const usersRef = require("../../../model-dbs/DBuser");
module.exports = function (req, res) {
  const usersRefR = usersRef.child("Recruiter").child("Post");
  const id = req.params.ID;
  usersRefR.child(id).update({
    lock: true,
  });
  res.redirect("/admin/post/" + id);
};
