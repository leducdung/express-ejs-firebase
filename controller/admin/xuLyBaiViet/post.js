module.exports = function (req, res) {
  var id = req.params.ID;
  const usersRef = require("../../../model-dbs/DBuser")
    .child("Recruiter")
    .child("Post")
    .child(id);
  const cookie_username = req.cookies.usernameADMIN;
  usersRef.once("value", (snapshot) => {
    let result = snapshot.val();
    res.render("admin/post", {
      cookie: cookie_username,
      result,
    });
  });
};
