const usersRef = require("../../../model-dbs/DBuser");
const usersRefR = usersRef.child("Recruiter").child("Profile");
const usersRefV = usersRef.child("Volunteer").child("Profile");
module.exports = async function (req, res) {
  var demR = 0,
    demV = 0,
    demP = 0;
  const cookie_username = req.cookies.usernameADMIN;
  if (cookie_username) {
    const query1 = usersRefR.orderByKey();
    const query2 = usersRefV.orderByKey();
    const query3 = usersRef.child("Recruiter").child("Post").orderByKey();
    await query1.once("value").then(function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        demR++;
      });
    });
    await query2.once("value").then(function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        demV++;
      });
    });
    await query3.once("value").then(function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        demP++;
      });
    });
    await res.render("admin/index", {
      demR,
      demV,
      demP,
      cookie:cookie_username,
    });
  } else {
    res.redirect("/admin/dangnhap");
  }
};
