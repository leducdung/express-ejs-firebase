//const usersRef = require("../../model-dbs/DBuser_V_P");
const usersRef_ADMIN = require("../../../model-dbs/DBAdmin");
module.exports = function (req, res) {
  const adminLogin = usersRef_ADMIN.child("profile");
  var arr = [];
  let { username } = req.body;
  let { password } = req.body;
  var errs = "";
  adminLogin.child(username).once("value", (snapshot) => {
    let result = snapshot.val();
    if (result == null) {
      const cookie = req.cookies.usernameADMIN;
      res.render("admin/login", {
        cookie: cookie,
        errs: "Tài khoản không tồn tại hoặc không đúng",
      });
    } else if (result.password != password) {
      const cookie = req.cookies.usernameADMIN;
      res.render("admin/login", {
        cookie: cookie,
        errs: "mật khẩu không đúng",
      });
    } else {
      res.cookie("usernameADMIN", username);
      res.cookie("admin", "admin");
      res.redirect("/admin/index");
    }
  });
};
