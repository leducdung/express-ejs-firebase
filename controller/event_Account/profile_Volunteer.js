const usersRef = require("../../../model-dbs/DBuser");

module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  const usersRefV = usersRef
    .child(cookie_user)
    .child("Profile")
    .child(cookie_username);
  usersRefV.orderByChild("username").once("value", (snapshot) => {
    let result = snapshot.val();
    res.render("user/profile_Volunteer", {
      cookie: cookie_username,
      cookie_user: cookie_user,
      result: result,
    });
  });
};
