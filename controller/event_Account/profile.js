const usersRef = require("../../model-dbs/DBuser");

module.exports = function (req, res, next) {
  const cookie_user = req.cookies.user;

  const cookie_username = req.cookies.username;
  var usernameNow = req.params.username;
  const usersRefNow = usersRef
    .child(cookie_user)
    .child("Profile")
    .child(usernameNow);
  const usersRefTheoDoi = usersRef
    .child("Volunteer")
    .child("Profile")
    .child(cookie_username)
    .child("theodoi");
  usersRefTheoDoi.once("value", (snap) => {});
  if (cookie_user === "Recruiter") {
    usersRefNow.orderByChild("username").once("value", (snapshot) => {
      let result = snapshot.val();
      if (result) {
        res.render("user/profile", {
          cookie: cookie_username,
          result: result,
          cookie_userNow: "Recruiter",
          usernameNow: usernameNow,
          cookie_user,
        });
      } else {
        const usersRefV = usersRef
          .child("Volunteer")
          .child("Profile")
          .child(usernameNow);
        usersRefV.orderByChild("username").once("value", (snapshot1) => {
          let result = snapshot1.val();
          res.render("user/profile", {
            cookie: cookie_username,
            result: result,
            cookie_userNow: "Volunteer",
            usernameNow: usernameNow,
            cookie_user,
          });
        });
      }
    });
  } else {
    usersRefNow.orderByChild("username").once("value", (snapshot) => {
      let result = snapshot.val();
      if (result) {
        res.render("user/profile", {
          cookie: cookie_username,
          result: result,
          cookie_userNow: "Volunteer",
          usernameNow: usernameNow,
          cookie_user,
        });
      } else {
        const usersRefR = usersRef
          .child("Recruiter")
          .child("Profile")
          .child(usernameNow);
        usersRefR.orderByChild("username").once("value", (snapshot1) => {
          let result = snapshot1.val();
          res.render("user/profile", {
            cookie: cookie_username,
            result: result,
            cookie_userNow: "Recruiter",
            usernameNow: usernameNow,
            cookie_user,
          });
        });
      }
    });
  }
};
