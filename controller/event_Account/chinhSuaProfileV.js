const usersRef_R_P = require("../../model-dbs/DBuser_V_P");
module.exports = function (req, res) {
  const username = req.params.username;
  usersRef_R_P.child(username).update({
    phone: req.body.Vphone,
    mail: req.body.Vmail,
    gender: req.body.Vgender,
    fullname: req.body.Vfullname,
  });
  res.redirect("/profile/" + username);
};
