const usersRef_R_P = require("../../../model-dbs/DBuser_R_P");
module.exports = function (req, res) {
  const username = req.params.username;
  usersRef_R_P.child(username).update({
    tentochuc: req.body.Rtentochuc,
    phone: req.body.Rphone,
    mail: req.body.Rmail,
    CMND: req.body.RCMND,
  });
  res.redirect("/profile/" + username);
};
