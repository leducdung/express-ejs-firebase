const usersRef = require("../../../model-dbs/DBuser");

module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  const usersRefV = usersRef.child(cookie_user).child("Post");
  console.log(req.body.fullname);
  const user_id = usersRefV.push().key;
  usersRefV.child(user_id).set({
    tenbaiviet: req.body.tenbaiviet,
    dia_diem: req.body.dia_diem,
    diabanhoatdong: req.body.diabanhoatdong,
    doituong: req.body.doituong,
    image: req.body.image,
    time_Start: req.body.time_Start,
    time_End: req.body.time_End,
    mota: req.body.mota,
    linhvuc: req.body.linhvuc,
    soluong: req.body.soluong,
    hoatdong: "",
    ID: user_id,
    username: cookie_username,
    ketthuc: false,
    pushnoti: false,
  });
  res.redirect("/post_Setting")
};
