const usersRef = require("../../../model-dbs/DBuser");
module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  const usersRef_post = usersRef.child(cookie_user).child("Post");
  usersRef_post.orderByKey();
  const idPost = req.params.id_Post;

  usersRef_post.once("value").then(function (snapshot) {
    var arr = [];
    snapshot.forEach(function (childSnapshot) {
      arr.push(childSnapshot.val());
    });
    res.render("user/recruiter/quanlyPost", {
      cookie: cookie_username,
      cookie_user: cookie_user,
      arr: arr,
      idPost,
    });
  });
};
