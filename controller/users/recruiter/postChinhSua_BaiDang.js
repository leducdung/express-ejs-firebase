const usersRef = require("../../../model-dbs/DBuser");

module.exports = function (req, res) {
  const usersRefV = usersRef.child("Recruiter").child("Post");
  const id_Post = req.params.id_Post;
  usersRefV.child(id_Post).update({
    tenbaiviet: req.body.tenbaiviet,
    dia_diem: req.body.dia_diem,
    diabanhoatdong: req.body.diabanhoatdong,
    doituong: req.body.doituong,
    image: req.body.image,
    time_Start: req.body.time_Start,
    time_End: req.body.time_End,
    mota: req.body.mota,
    linhvuc: req.body.linhvuc,
    soluong: req.body.soluong,
  });
  res.redirect("/post_Setting/chinhsua/" + id_Post);
};
