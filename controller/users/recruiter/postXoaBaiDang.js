const usersRef = require("../../../model-dbs/DBuser");

module.exports = function (req, res) {
  const usersRefV = usersRef.child("Recruiter").child("Post");

  const usersRefAD = require("../../../model-dbs/DBAdmin").child("feedback");
  const cookie_username = req.cookies.usernameADMIN;
  var arr = [];
  usersRefAD.child(req.body.xoaIdNay).once("value", (snapshot) => {
    if (snapshot.val()) {
      usersRefV.child(req.body.xoaIdNay).remove();
      usersRefAD.child(req.body.xoaIdNay).remove()
      res.redirect("/post_Setting");
    } else {
      usersRefV.child(req.body.xoaIdNay).remove();
      res.redirect("/post_Setting");
    }
  });
};
