module.exports = function (req, res) {
  const cookie = req.cookies.username;
  const cookie_user = req.cookies.user;
  res.render("user/trangchu", {
    cookie: cookie,
    cookie_user: cookie_user,
  });
};
