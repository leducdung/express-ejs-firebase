const usersRef = require("../../../model-dbs/DBuser");
module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  const usersRef_post = usersRef.child("Recruiter").child("Post");
  usersRef_post.orderByKey();
  usersRef_post.once("value").then(function (snapshot) {
    var arr = [];
    snapshot.forEach(function (childSnapshot) {
      arr.push(childSnapshot.val());
    });
    var arr2 = [];
    arr.forEach(function (arr) {
      if (arr2.includes(arr.linhvuc) == false) arr2.push(arr.linhvuc);
    });
    res.render("user/nav/danhbaphiloinhuan", {
      cookie: cookie_username,
      cookie_user: cookie_user,
      arr2: arr2,
      arr: arr,
    });
  });
};
