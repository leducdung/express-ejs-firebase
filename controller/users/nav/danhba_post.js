const usersRef = require("../../../model-dbs/DBuser");
module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  const { doiTuongTG, diaBanHD } = req.query;
  const usersRef_post = usersRef.child("Recruiter").child("Post");
  usersRef_post.orderByKey();
  usersRef_post.once("value").then(function (snapshot) {
    var arr = [];
    snapshot.forEach(function (childSnapshot) {
      arr.push(childSnapshot.val());
    });
    
    if(doiTuongTG){
      res.render("user/volunteer/timKiem", {
        cookie: cookie_username,
        cookie_user: cookie_user,
        arr: arr,
        doiTuongTG,
        diaBanHD,
        linhvuc: req.params.linhvuc,
      });
    }else{
      res.render("user/nav/danhba_post", {
        cookie: cookie_username,
        cookie_user: cookie_user,
        arr: arr,
        doiTuongTG,
        diaBanHD,
        linhvuc: req.params.linhvuc,
      });
    }
  });
};
