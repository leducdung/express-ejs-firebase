module.exports = function (req, res) {
  var username = req.body.username;
  res.redirect("/profile/" + username);
};
