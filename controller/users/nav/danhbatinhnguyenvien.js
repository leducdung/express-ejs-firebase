const usersRef = require("../../../model-dbs/DBuser")
  .child("Volunteer")
  .child("Profile");
module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  var arr = [];
  usersRef.once("value").then(function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
      arr.push(childSnapshot.val());
    });
    res.render("user/nav/danhbatinhnguyenvien", {
      cookie: cookie_username,
      arr,
      cookie_user,
    });
  });
};
