const usersRef = require("../../../model-dbs/DBuser")
  .child("Recruiter")
  .child("Profile");
module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  var arr = [];
  usersRef.once("value").then(function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
      var key = childSnapshot.key;

      arr.push(childSnapshot.val());
    });
    res.render("user/nav/cohoitinhnguyen", {
      cookie: cookie_username,
      arr,
      cookie_user,
    });
  });
};
