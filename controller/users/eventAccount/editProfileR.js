const usersRef = require("../../model-dbs/DBuser");

module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  var usernameNow = req.params.username;
  const usersRefNow = usersRef
    .child(cookie_user)
    .child("Profile")
    .child(usernameNow);
  usersRefNow.orderByChild("username").on("value", (snapshot) => {
    let result = snapshot.val();
    if (result) {
      res.render("profile/editProfileR", {
        cookie: cookie_username,
        result: result,
        cookie_userNow: "Recruiter",
        usernameNow: usernameNow,
        cookie_user,
      });
    }
  });
};
