const usersRef = require("../../../model-dbs/DBuser");
module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  const usersRef_post = usersRef.child("Recruiter").child("Post");

  usersRef_post.orderByKey();
  usersRef_post.once("value").then(function (snapshot) {
    var arr = [];
    snapshot.forEach(function (childSnapshot) {
      //   let usersRef_post_key = usersRef_post.child(childSnapshot.key);
      //   usersRef_post_key.orderByKey.once("value").then(function (snapshot2) {
      //     snapshot2.forEach(function (childSnapshot2) {
      arr.push(childSnapshot.val());
      //     });
      // });
    });
    res.render("user/volunteer/volunteer_Setting", {
      cookie: cookie_username,
      cookie_user: cookie_user,
      arr: arr,
    });
  });
};
