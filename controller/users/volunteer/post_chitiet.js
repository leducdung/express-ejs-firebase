const usersRef = require("../../../model-dbs/DBuser");
module.exports = function (req, res) {
  const cookie_user = req.cookies.user;
  const cookie_username = req.cookies.username;
  var id_Post = req.params.id_Post;
  var linhvuc = req.params.linhvuc;
  const usersRef_post = usersRef.child("Recruiter").child("Post");
  const usersRef_hoatDong = usersRef
    .child("Recruiter")
    .child("Post")
    .child(id_Post)
    .child("hoatdong");
  var arrHD = [];
  usersRef_hoatDong.orderByKey();
  usersRef_hoatDong.once("value").then(function (snapshotHD) {
    snapshotHD.forEach(function (childSnapshotHD) {
      arrHD.push(childSnapshotHD.val());
    });
    usersRef_post.orderByKey();
    usersRef_post.once("value").then(function (snapshot) {
      var arr = [];
      snapshot.forEach(function (childSnapshot) {
        arr.push(childSnapshot.val());
      });
      res.render("user/chiTiet_BaiDang", {
        cookie: cookie_username,
        cookie_user: cookie_user,
        arr: arr,
        id_Post: id_Post,
        linhvuc:linhvuc,
        arrHD: arrHD,
      });
    });
  });
};
