const usersRef_ADMIN = require("../../../model-dbs/DBAdmin");
module.exports = function (req, res) {
    var {idpost,rate,phanHoi,usernameFB} = req.body;
    var {ID,linhvuc} = req.params ;
    const usersRegister = usersRef_ADMIN.child("feedback").child(idpost);
    usersRegister.child(usernameFB).set({
        idpost: idpost,
        rate: parseInt(rate),
        phanHoi:phanHoi,
        usernameFB:usernameFB,
    });
    res.redirect("/danhba_post/"+linhvuc+"/"+ID);
    
};
