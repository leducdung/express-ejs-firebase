const usersRef = require("../../../model-dbs/DBuser");

module.exports = function (req, res) {
  var ID = req.body.IDxoaNhap;
  const cookie = req.cookies.username;
  const usersRefV = usersRef
    .child("Recruiter")
    .child("Post")
    .child(ID)
    .child("hoatdong");
  usersRefV.child(cookie).remove();
  res.redirect("/Volunteer_Setting");
};
