//const usersRef = require("../../../model-dbs/DBuser_V_P");
const usersRef = require("../../../model-dbs/DBuser");
const usersRefR = usersRef.child("Recruiter").child("Profile");
const usersRefV = usersRef.child("Volunteer").child("Profile");
module.exports = function (req, res) {
  var arr = [];
  let { username } = req.body;
  let { password } = req.body;
  var errs = "";
  usersRefR
    .orderByChild("username")
    .equalTo(username)
    .on("value", (snapshot) => {
      let result = snapshot.val();
      if (result == null) {
        usersRefV
          .orderByChild("username")
          .equalTo(username)
          .on("value", (snapshot) => {
            let result1 = snapshot.val();
            if (result1 == null) {
              const cookie = req.cookies.username;
              res.render("user/account/dangnhap", {
                cookie: cookie,
                errs: "Tài khoản không tồn tại",
              });
            } else {
              usersRefV.child(username).orderByKey();
              usersRefV
                .child(username)
                .once("value")
                .then(function (snapshot) {
                  snapshot.forEach(function (childSnapshot) {
                    arr.push(childSnapshot.val());
                  });
                  if (arr[9] != password) {
                    const cookie = req.cookies.username;
                    res.render("user/account/dangnhap", {
                      cookie: cookie,
                      errs: "mật khẩu không đúng",
                    });
                  } else if (arr[7] == true) {
                    const cookie = req.cookies.username;
                    res.render("user/account/dangnhap", {
                      cookie: cookie,
                      errs: "Tài khoản này đã bị khoá",
                    });
                  } else {
                    res.cookie("username", username);
                    res.cookie("user", "Volunteer");
                    res.redirect("trangchu");
                  }
                });
            }
          });
      } else {
        usersRefR.child(username).orderByKey();
        usersRefR
          .child(username)
          .once("value")
          .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
              arr.push(childSnapshot.val());
            });
            if (arr[6] != password) {
              const cookie = req.cookies.username;
              res.render("user/account/dangnhap", {
                cookie: cookie,
                errs: "mật khẩu không đúng",
              });
            } else if (arr[2] == true) {
              const cookie = req.cookies.username;
              res.render("user/account/dangnhap", {
                cookie: cookie,
                errs: "Tài khoản này đã bị khoá",
              });
            } else {
              res.cookie("username", username);
              res.cookie("user", "Recruiter");
              res.redirect("trangchu");
            }
          });
      }
    });
};
