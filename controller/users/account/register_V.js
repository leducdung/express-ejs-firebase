const usersRef1 = require("../../../model-dbs/DBuser_V_P");
module.exports = async function (req, res) {
  var errs = "";
  var kiemtraV, kiemtraR;
  const usersRef = require("../../../model-dbs/DBuser");
  const usersRefR = usersRef.child("Recruiter").child("Profile");
  const usersRefV = usersRef.child("Volunteer").child("Profile");
  const username = req.body.username;
  // let user_id = usersRef.push().key;
  await usersRefR
    .orderByChild("username")
    .equalTo(username)
    .once("value", (snapshot1) => {
      kiemtraR = snapshot1.val();
    });
  await usersRefV
    .orderByChild("username")
    .equalTo(username)
    .once("value", (snapshot) => {
      kiemtraV = snapshot.val();
    });
  if (kiemtraV != null || kiemtraR != null) {
    const cookie = req.cookies.username;
    const cookie_user = req.cookies.user;
    res.render("user/account/dangky", {
      cookie: cookie,
      cookie_user: cookie_user,
      errs: "tài khoản này đã tồn tại",
    });
  } else {
    usersRef1.child(req.body.username).set({
      address: "null",
      avatar: "null",
      birthday: "null",
      cmnd: 12345679,
      theodoi: "null",
      username: req.body.username,
      phone: req.body.phone,
      password: req.body.password,
      mail: req.body.mail,
      gender: req.body.gender,
      fullname: req.body.fullname,
      diemTinhNguyen: 0,
      lock: false,
    });
    const cookie = req.cookies.username;
    const cookie_user = req.cookies.user;
    res.render("user/account/dangnhap", {
      cookie: cookie,
      cookie_user: cookie_user,
      errs: "",
    });
  }
};
