module.exports = function (req, res) {
  let errs = "";
  const cookie = req.cookies.username;
  const cookie_user = req.cookies.user;
  if (cookie) {
    res.render("user/trangchu", {
      cookie: cookie,
      cookie_user: cookie_user,
    });
  }else{
    res.render("user/account/dangnhap", {
      cookie: cookie,
      cookie_user: cookie_user,
      errs: errs,
    });
  }
  
};
