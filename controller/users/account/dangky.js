module.exports = function (req, res) {
  var errs = "";
  const cookie = req.cookies.username;
  const cookie_user = req.cookies.user;
  res.render("user/account/dangky", {
    cookie: cookie,
    cookie_user: cookie_user,
    errs,
  });
};
