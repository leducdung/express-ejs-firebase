const admin = require("firebase-admin");
const serviceAccount = require("../phuc.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://capstone1-27ec7.firebaseio.com"
  });

  module.exports  = admin.database();