const express = require("express");
const session = require("express-session");
var cookieParser = require("cookie-parser");
const multer = require("multer");
const bodyParser = require("body-parser");
const router = express.Router();
var { check, expressValidator } = require("express-validator");
const urlLenCodeParser = bodyParser.urlencoded({ extended: false });

const cohoitinhnguyen = require("../controller/users/nav/cohoitinhnguyen");
const dangnhap = require("../controller/users/account/dangnhap");
const danhbaphiloinhuan = require("../controller/users/nav/danhbaphiloinhuan");
const danhbatinhnguyenvien = require("../controller/users/nav/danhbatinhnguyenvien");
const timKiemNguoiDung = require("../controller/users/nav/timKiemNguoiDung");
const profile = require("../controller/event_Account/profile");
const trangchu = require("../controller/users/trangchu");
const dangky = require("../controller/users/account/dangky");
const register_V = require("../controller/users/account/register_V");
const register_R = require("../controller/users/account/register_R");
const login = require("../controller/users/account/login");
const quenmk = require("../controller/users/account/quenmk");
const mkmoi = require("../controller/users/account/mkmoi");
const layLaiMk = require("../controller/users/account/layLaiMk");
const logout = require("../controller/users/account/logout");
const post_Setting = require("../controller/users/recruiter/post_Setting");
const danhba_post = require("../controller/users/nav/danhba_post");
const Post_R = require("../controller/users/recruiter/Post_R");
const theodoi = require("../controller/users/volunteer/theodoi");
const post_chitiet = require("../controller/users/volunteer/post_chitiet");
const thamgiahoatdong = require("../controller/users/volunteer/thamgiahoatdong");
const huyThamGiaHD = require("../controller/users/volunteer/huyThamGiaHD");
const anHoatDongTrenLS = require("../controller/users/volunteer/anHoatDongTrenLS");
const lichsu = require("../controller/users/volunteer/lichsu");
const timKiem = require("../controller/users/timKiem");

const volunteer_Setting = require("../controller/users/volunteer/volunteer_Setting");
const getChinhSua = require("../controller/users/recruiter/getChinhSua");
const postChinhSua_BaiDang = require("../controller/users/recruiter/postChinhSua_BaiDang");
const postXoaBaiDang = require("../controller/users/recruiter/postXoaBaiDang");
const quanlyPost = require("../controller/users/recruiter/quanlyPost");
const ktHoatDong = require("../controller/users/recruiter/ketThucHoatDong");
const rateV = require("../controller/users/recruiter/rateV");

const editProfileR = require("../controller/event_Account/editProfileR");
const chinhSuaProfileR = require("../controller/users/eventAccount/chinhSuaProfileR");
const chinhSuaProfileV = require("../controller/event_Account/chinhSuaProfileV");
const doimkR = require("../controller/event_Account/doimkR");
const doimkV = require("../controller/event_Account/doimkV");

//admin

const adminDangnhap = require("../controller/admin/taiKhoanAdmin/dangnhap");
const adminForgot_Password = require("../controller/admin/taiKhoanAdmin/forgot-password");
const adminDangKy = require("../controller/admin/taiKhoanAdmin/dangky");
const adminIndex = require("../controller/admin/xuLyBaiViet/index");
const adminManagepost = require("../controller/admin/xuLyBaiViet/managepost");
const adminPost = require("../controller/admin/xuLyBaiViet/post");
const adminAccountV = require("../controller/admin/xuLyTaiKhoan/accountV");
const adminAccountR = require("../controller/admin/xuLyTaiKhoan/accountR");
const adminaccountAD = require("../controller/admin/taiKhoanAdmin/accountAD");
const adminManageaccountV = require("../controller/admin/xuLyTaiKhoan/manageaccountV");
const adminManageaccountR = require("../controller/admin/xuLyTaiKhoan/manageaccountR");
const adminE404 = require("../controller/admin/404");
const baocao = require("../controller/admin/baocao");

const adminRegister = require("../controller/admin/taiKhoanAdmin/register");
const adminLogin = require("../controller/admin/taiKhoanAdmin/login");
const adminLogout = require("../controller/admin/taiKhoanAdmin/logout");
const rLockAccount = require("../controller/admin/xuLyTaiKhoan/rLockAccount");
const vLockAccount = require("../controller/admin/xuLyTaiKhoan/vLockAccount");
const rUnlockAccount = require("../controller/admin/xuLyTaiKhoan/rUnlockAccount");
const vUnlockAccount = require("../controller/admin/xuLyTaiKhoan/vUnlockAccount");
const postLock = require("../controller/admin/xuLyBaiViet/postLock");
const postUnlock = require("../controller/admin/xuLyBaiViet/postUnlock");
const feedBack = require("../controller/users/volunteer/feedBack")
const postXoaBaiDangPH = require("../controller/admin/xuLyBaiViet/postXoaBaiDangPH");
const doimk = require("../controller/admin/taiKhoanAdmin/doimk");
const doimkPost = require("../controller/admin/taiKhoanAdmin/doimkPost");

const bot = require("../controller/chatBot/bot");

router.use(bodyParser());
var sess;
router.use(
  session({
    secret: "doraemon",
    resave: true,
    saveUninitialized: true,
    cookie: {
      maxAge: 1000 * 60 * 100000,
    },
  })
);
router.use(cookieParser());

/* GET home page. */

router.get("/cohoitinhnguyen", cohoitinhnguyen);
router.get("/dangnhap", dangnhap);
router.get("/quenmk", quenmk);
router.get("/mkmoi/:gmail", mkmoi);
router.get("/danhbaphiloinhuan", danhbaphiloinhuan);
router.get("/danhbatinhnguyenvien", danhbatinhnguyenvien);
router.get("/profile/:username", profile);
router.get("/dangky", dangky);
router.get("/trangchu", trangchu);
router.get("/post_Setting/chinhsua/:idPost", getChinhSua);
router.get("/profile/editProfileR/:username", editProfileR);
router.get("/lichsu", lichsu);

router.get("/post_Setting", post_Setting);
router.get("/volunteer_Setting", volunteer_Setting);

router.get("/danhba_post/:linhvuc", danhba_post);
router.get("/danhba_post/timkiem", timKiem);
router.get("/danhba_post/:linhvuc/:id_Post", post_chitiet);
router.get("/post_Setting/:id_Post", quanlyPost);

router.post("/theodoi/:username", theodoi);
router.post("/feedBack/:linhvuc/:ID", feedBack);

router.post("/register_R", register_R);
router.post("/logout", logout);
router.post("/register_V", register_V);
router.post("/login", login);
router.post("/Post_R", Post_R);
router.post("/timKiemNguoiDung", timKiemNguoiDung);

router.post("/layLaiMk/:gmail", layLaiMk);
router.post("/thamgiahoatdong/:id_Post", thamgiahoatdong);
router.post("/Volunteer_Setting/huythamgia", huyThamGiaHD);
router.post("/post_Setting/chinhsua1/:id_Post", postChinhSua_BaiDang);
router.post("/post_Setting/xoa", postXoaBaiDang);
router.post("/postXoaBaiDangPH/xoa", postXoaBaiDangPH);
router.post("/post_Setting/:id_Post/:username", rateV);
router.post("/profile/:username/chinhSuaProfileR", chinhSuaProfileR);
router.post("/profile/:username/chinhSuaProfileV", chinhSuaProfileV);
router.post("/profile/:username/doimkR", doimkR);
router.post("/profile/:username/doimkV", doimkV);
router.post("/Volunteer_Setting/anHoatDongTrenLS", anHoatDongTrenLS);
router.post("/post_Setting/kthoatdong", ktHoatDong);

// admin

router.get("/admin/dangnhap", adminDangnhap);
router.get("/admin/adminaccountAD/doimk/:username", doimk);
router.get("/admin/404", adminE404);
router.get("/admin/baocao", baocao);
router.get("/admin/accountV/:username", adminAccountV);
router.get("/admin/accountR/:username", adminAccountR);
router.get("/admin/adminaccountAD/:username", adminaccountAD);
router.get("/admin/forgot-password", adminForgot_Password);
router.get("/admin/index", adminIndex);
router.get("/admin/manageaccountV", adminManageaccountV);
router.get("/admin/manageaccountR", adminManageaccountR);
router.get("/admin/managepost", adminManagepost);
router.get("/admin/post/:ID", adminPost);
router.get("/admin/dangky", adminDangKy);
router.post("/admin/register", adminRegister);
router.post("/admin/login", adminLogin);
router.post("/admin/logout", adminLogout);
router.post("/admin/adminaccountAD/doimkpost/:username", doimkPost);
router.post("/admin/lock/accountR/:username", rLockAccount);
router.post("/admin/lock/accountV/:username", vLockAccount);
router.post("/admin/unLock/accountR/:username", rUnlockAccount);
router.post("/admin/unLock/accountV/:username", vUnlockAccount);
router.post("/admin/lock/post/:ID", postLock);
router.post("/admin/unLock/post/:ID", postUnlock);

// bot

router.get("/bot", bot);

module.exports = router;
